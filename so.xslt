<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:output omit-xml-declaration="yes" method="html"/>
<xsl:strip-space elements="*"/>

<xsl:template match="Document">
    <html>
     <head><title>Test</title></head>
        <xsl:apply-templates/>
    </html>
</xsl:template>

<xsl:template match="link">
    <body>
        <xsl:apply-templates/><hr/>
        Page:<xsl:value-of select="."/>
    </body>
</xsl:template>

<xsl:template match="description">
  <xsl:apply-templates/><hr/>
    <br/><xsl:value-of select="."/><br/>
</xsl:template>

<xsl:template match="nameLink">
    <xsl:value-of select="."/><br/>
</xsl:template>

<xsl:template match="*">
<xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
